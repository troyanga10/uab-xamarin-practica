﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CalPrac
{
    public class VMLocator
    {
        private Lazy<CalculatorViewModel> mainViewModel;

        public VMLocator()
        {
            mainViewModel = new Lazy<CalculatorViewModel>(() => new CalculatorViewModel());
        }

        public CalculatorViewModel MainViewModel
        {
            get
            {
                return mainViewModel.Value;
            }
        }
    }
}
