﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Text;
using System.Windows.Input;
using Xamarin.Forms;

namespace CalPrac
{
    public class CalculatorViewModel : INotifyPropertyChanged
    {
        string entry = "0";
        Stack<double> stack = new Stack<double>();

        public event PropertyChangedEventHandler PropertyChanged;

        public CalculatorViewModel()
        {
            ClearCommand = new Command(
                execute: () =>
                {
                    stack.Clear();
                    Entry = "0";
                    RefreshCanExecutes();
                    RefreshStackDisplay();
                });

            ClearEntryCommand = new Command(
                execute: () =>
                {
                    Entry = "0";
                    RefreshCanExecutes();
                });


            DigitCommand = new Command<string>(
                execute: (string arg) =>
                {
                    Entry += arg;
                    if (Entry.StartsWith("0") && !Entry.StartsWith("0."))
                    {
                        Entry = Entry.Substring(1);
                    }
                    RefreshCanExecutes();
                },
                canExecute: (string arg) =>
                {
                    return !(arg == "." && Entry.Contains("."));
                });

            EnterCommand = new Command(
                execute: () =>
                {
                    stack.Push(double.Parse(Entry));
                    Entry = "0";
                    RefreshStackDisplay();
                    RefreshCanExecutes();
                });

            BinaryOperation = new Command<string>(
                execute: (string op) =>
                {
                    double x = stack.Pop();
                    double y = stack.Pop();
                    double result = 0;
                    switch (op)
                    {
                        case "divide": result = y / x; break;
                        case "multiply": result = y * x; break;
                        case "subtract": result = y - x; break;
                        case "add": result = y + x; break;
                        case "menor":
                            if (x < y) result = x;
                            else result = y; break;
                    }

                    stack.Push(result);
                    RefreshCanExecutes();
                    RefreshStackDisplay();
                },
                canExecute: (string op) =>
                {
                    return stack.Count > 1;
                });
        }

        void RefreshCanExecutes()
        {
            ((Command)DigitCommand).ChangeCanExecute();
            ((Command)BinaryOperation).ChangeCanExecute();
        }

        void RefreshStackDisplay()
        {
            OnPropertyChanged("XStackValue");
            OnPropertyChanged("YStackValue");
        }

        public string Entry
        {
            private set { SetProperty(ref entry, value); }
            get { return entry; }
        }

        public string XStackValue
        {
            get { return stack.Count > 0 ? stack.Peek().ToString() : ""; }
        }

        public string YStackValue
        {
            get
            {
                string result = "";

                if (stack.Count > 1)
                {
                    double hold = stack.Pop();
                    result = stack.Peek().ToString();
                    stack.Push(hold);
                }

                return result;
            }
        }

        public ICommand ClearCommand { private set; get; }

        public ICommand ClearEntryCommand { private set; get; }

        public ICommand DigitCommand { private set; get; }

        public ICommand EnterCommand { private set; get; }

        public ICommand BinaryOperation { private set; get; }

        bool SetProperty<T>(ref T storage, T value, [CallerMemberName] string propertyName = null)
        {
            if (Object.Equals(storage, value))
                return false;

            storage = value;
            OnPropertyChanged(propertyName);
            return true;
        }

        protected void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

    }
}
