package md529a2523497ac1abb2af3e5d2402bae44;


public class C1CheckBoxRenderer
	extends md529a2523497ac1abb2af3e5d2402bae44.C1ViewRenderer_2
	implements
		mono.android.IGCUserPeer
{
/** @hide */
	public static final String __md_methods;
	static {
		__md_methods = 
			"";
		mono.android.Runtime.register ("C1.Xamarin.Forms.Core.Platform.Android.C1CheckBoxRenderer, C1.Xamarin.Forms.Core.Platform.Android, Version=2.5.20173.241, Culture=neutral, PublicKeyToken=null", C1CheckBoxRenderer.class, __md_methods);
	}


	public C1CheckBoxRenderer (android.content.Context p0, android.util.AttributeSet p1, int p2)
	{
		super (p0, p1, p2);
		if (getClass () == C1CheckBoxRenderer.class)
			mono.android.TypeManager.Activate ("C1.Xamarin.Forms.Core.Platform.Android.C1CheckBoxRenderer, C1.Xamarin.Forms.Core.Platform.Android, Version=2.5.20173.241, Culture=neutral, PublicKeyToken=null", "Android.Content.Context, Mono.Android, Version=0.0.0.0, Culture=neutral, PublicKeyToken=84e04ff9cfb79065:Android.Util.IAttributeSet, Mono.Android, Version=0.0.0.0, Culture=neutral, PublicKeyToken=84e04ff9cfb79065:System.Int32, mscorlib, Version=2.0.5.0, Culture=neutral, PublicKeyToken=7cec85d7bea7798e", this, new java.lang.Object[] { p0, p1, p2 });
	}


	public C1CheckBoxRenderer (android.content.Context p0, android.util.AttributeSet p1)
	{
		super (p0, p1);
		if (getClass () == C1CheckBoxRenderer.class)
			mono.android.TypeManager.Activate ("C1.Xamarin.Forms.Core.Platform.Android.C1CheckBoxRenderer, C1.Xamarin.Forms.Core.Platform.Android, Version=2.5.20173.241, Culture=neutral, PublicKeyToken=null", "Android.Content.Context, Mono.Android, Version=0.0.0.0, Culture=neutral, PublicKeyToken=84e04ff9cfb79065:Android.Util.IAttributeSet, Mono.Android, Version=0.0.0.0, Culture=neutral, PublicKeyToken=84e04ff9cfb79065", this, new java.lang.Object[] { p0, p1 });
	}


	public C1CheckBoxRenderer (android.content.Context p0)
	{
		super (p0);
		if (getClass () == C1CheckBoxRenderer.class)
			mono.android.TypeManager.Activate ("C1.Xamarin.Forms.Core.Platform.Android.C1CheckBoxRenderer, C1.Xamarin.Forms.Core.Platform.Android, Version=2.5.20173.241, Culture=neutral, PublicKeyToken=null", "Android.Content.Context, Mono.Android, Version=0.0.0.0, Culture=neutral, PublicKeyToken=84e04ff9cfb79065", this, new java.lang.Object[] { p0 });
	}

	private java.util.ArrayList refList;
	public void monodroidAddReference (java.lang.Object obj)
	{
		if (refList == null)
			refList = new java.util.ArrayList ();
		refList.add (obj);
	}

	public void monodroidClearReferences ()
	{
		if (refList != null)
			refList.clear ();
	}
}
